<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');
    
    $sheep = new Animal("shaun");

    echo "Nama Hewan : $sheep->name  <br>"; // "shaun"
    echo "Jumlah Kaki : $sheep->legs <br>"; // 4
    echo "Berdarah dingin : $sheep->cold_blooded <br>"; // "no"

    $sungokong = new Ape("kera sakti");
    echo "<br>Nama Hewan : $sungokong->name  <br>"; 
    echo "Jumlah Kaki : $sungokong->legs <br>"; 
    echo "Berdarah dingin : $sungokong->cold_blooded <br>";
    $sungokong->yell(); // "Auooo"

    $kodok = new Frog("buduk");
    echo "<br>Nama Hewan : $kodok->name  <br>"; 
    echo "Jumlah Kaki : $kodok->legs <br>"; 
    echo "Berdarah dingin : $kodok->cold_blooded <br>";
    $kodok->jump() ; // "hop hop"


// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>